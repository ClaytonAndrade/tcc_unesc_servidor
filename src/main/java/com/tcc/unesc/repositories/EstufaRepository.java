package com.tcc.unesc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tcc.unesc.model.EstufaModel;

@Repository
public interface EstufaRepository  extends JpaRepository<EstufaModel, Integer>{

}
