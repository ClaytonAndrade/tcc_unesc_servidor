package com.tcc.unesc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tcc.unesc.model.SensorPhModel;

@Repository
public interface SensorPhRepository  extends JpaRepository<SensorPhModel, Integer>{

}
