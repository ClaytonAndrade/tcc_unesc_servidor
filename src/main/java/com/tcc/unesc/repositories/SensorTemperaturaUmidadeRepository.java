package com.tcc.unesc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tcc.unesc.model.SensorTemperaturaUmidadeModel;

@Repository
public interface SensorTemperaturaUmidadeRepository  extends JpaRepository<SensorTemperaturaUmidadeModel, Integer>{

}
