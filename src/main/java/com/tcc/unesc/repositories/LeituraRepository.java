package com.tcc.unesc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tcc.unesc.model.LeituraModel;

import java.util.List;

@Repository
public interface LeituraRepository  extends JpaRepository<LeituraModel, Integer>{

	@Query(value = "SELECT MAX(obj.idLeitura) FROM LeituraModel obj")
	public Integer max();
//, obj.tipoLeitura, obj.valorObtido, obj.identificacaoSensorEnum
	@Query(value = "select obj.idLeitura from LeituraModel obj where obj.idLeitura in \n" +
			"\t\t( SELECT MAX(obj2.idLeitura) FROM LeituraModel obj2 where obj.estufaModel.idEstufa = ?1 and obj2.identificacaoSensorEnum = 0  ) or obj.idLeitura in \n" +
			"\t\t( SELECT MAX(obj2.idLeitura) FROM LeituraModel obj2 where obj.estufaModel.idEstufa = ?1 and obj2.identificacaoSensorEnum = 1  ) or obj.idLeitura in \n" +
			"\t\t( SELECT MAX(obj2.idLeitura) FROM LeituraModel obj2 where obj.estufaModel.idEstufa = ?1 and obj2.identificacaoSensorEnum = 2  ) or obj.idLeitura in \n" +
			"\t\t( SELECT MAX(obj2.idLeitura) FROM LeituraModel obj2 where obj.estufaModel.idEstufa = ?1 and obj2.identificacaoSensorEnum = 3  ) or obj.idLeitura in \n" +
			"\t\t( SELECT MAX(obj2.idLeitura) FROM LeituraModel obj2 where obj.estufaModel.idEstufa = ?1 and obj2.identificacaoSensorEnum = 4  ) or obj.idLeitura in \n" +
			"\t\t( SELECT MAX(obj2.idLeitura) FROM LeituraModel obj2 where obj.estufaModel.idEstufa = ?1 and obj2.identificacaoSensorEnum = 5  ) or obj.idLeitura in \n" +
			"\t\t( SELECT MAX(obj2.idLeitura) FROM LeituraModel obj2 where obj.estufaModel.idEstufa = ?1 and obj2.identificacaoSensorEnum = 6  ) or obj.idLeitura in \n" +
			"\t\t( SELECT MAX(obj2.idLeitura) FROM LeituraModel obj2 where obj.estufaModel.idEstufa = ?1 and obj2.identificacaoSensorEnum = 7  ) or obj.idLeitura in \n" +
			"\t\t( SELECT MAX(obj2.idLeitura) FROM LeituraModel obj2 where obj.estufaModel.idEstufa = ?1 and obj2.identificacaoSensorEnum = 8  ) or obj.idLeitura in \n" +
			"\t\t( SELECT MAX(obj3.idLeitura) FROM LeituraModel obj3 where obj.estufaModel.idEstufa = ?1 and obj3.identificacaoSensorEnum = 9 ) ")
	public List<Integer> maxSensoresId(Integer idEstufa);

	@Query(value = "SELECT MAX(obj.idLeitura) FROM LeituraModel obj where obj.estufaModel.idEstufa = ?1 and obj.identificacaoSensorEnum = ?2 ")
	public Integer maxSensoresIdIdentificacaoEnum(Integer idEstufa, Integer identificacaoSensorEnum);


}

