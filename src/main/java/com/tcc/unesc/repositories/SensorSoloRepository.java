package com.tcc.unesc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tcc.unesc.model.SensorSoloModel;

@Repository
public interface SensorSoloRepository  extends JpaRepository<SensorSoloModel, Integer>{

}
