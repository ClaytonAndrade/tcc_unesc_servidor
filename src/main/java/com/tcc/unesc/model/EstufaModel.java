package com.tcc.unesc.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "estufas")
public class EstufaModel implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idEstufa;
	private String descricao;
	
	@JsonIgnore
	@OneToMany(mappedBy="estufaModel")
	private List<SensorPhModel> sensorPhModel = new ArrayList<>();
	
	@JsonIgnore
	@OneToMany(mappedBy="estufaModel")
	private List<SensorSoloModel> sensorSoloModel = new ArrayList<>();
	
	@JsonIgnore
	@OneToMany(mappedBy="estufaModel")
	private List<SensorTemperaturaUmidadeModel> sensorTemperaturaUmidadeModel = new ArrayList<>();
	
	@JsonIgnore
	@OneToMany(mappedBy="estufaModel")
	private List<LeituraModel> leituraModels = new ArrayList<>();

	public EstufaModel() {
	}

	public EstufaModel(Integer idEstufa, String descricao) {
		super();
		this.idEstufa = idEstufa;
		this.descricao = descricao;

	}

	public Integer getIdEstufa() {
		return idEstufa;
	}

	public void setIdEstufa(Integer idEstufa) {
		this.idEstufa = idEstufa;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<SensorPhModel> getSensorPhModel() {
		return sensorPhModel;
	}

	public void setSensorPhModel(List<SensorPhModel> sensorPhModel) {
		this.sensorPhModel = sensorPhModel;
	}

	public List<SensorSoloModel> getSensorSoloModel() {
		return sensorSoloModel;
	}

	public void setSensorSoloModel(List<SensorSoloModel> sensorSoloModel) {
		this.sensorSoloModel = sensorSoloModel;
	}

	public List<SensorTemperaturaUmidadeModel> getSensorTemperaturaUmidadeModel() {
		return sensorTemperaturaUmidadeModel;
	}

	public void setSensorTemperaturaUmidadeModel(List<SensorTemperaturaUmidadeModel> sensorTemperaturaUmidadeModel) {
		this.sensorTemperaturaUmidadeModel = sensorTemperaturaUmidadeModel;
	}

	
	public List<LeituraModel> getLeituraModels() {
		return leituraModels;
	}

	public void setLeituraModels(List<LeituraModel> leituraModels) {
		this.leituraModels = leituraModels;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idEstufa == null) ? 0 : idEstufa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EstufaModel other = (EstufaModel) obj;
		if (idEstufa == null) {
			if (other.idEstufa != null)
				return false;
		} else if (!idEstufa.equals(other.idEstufa))
			return false;
		return true;
	}
	
}
