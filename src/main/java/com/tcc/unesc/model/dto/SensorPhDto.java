package com.tcc.unesc.model.dto;

import com.tcc.unesc.model.EstufaModel;

import java.io.Serializable;

public class SensorPhDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer idSensor;
    private Double valorInformado;
    private Double percentualVariacao;
    private String acao;

    private EstufaModel estufaModel;

    public SensorPhDto() {
    }

    public SensorPhDto(Integer idSensor, Double valorInformado, Double percentualVariacao,
                       String acao, EstufaModel estufaModel) {
        super();
        this.idSensor = idSensor;
        this.valorInformado = valorInformado;
        this.percentualVariacao = percentualVariacao;
        this.acao = acao;
        this.estufaModel = estufaModel;
    }

    public Integer getIdSensor() {
        return idSensor;
    }

    public void setIdSensor(Integer idSensor) {
        this.idSensor = idSensor;
    }

    public Double getValorInformado() {
        return valorInformado;
    }

    public void setValorInformado(Double valorInformado) {
        this.valorInformado = valorInformado;
    }

    public Double getPercentualVariacao() {
        return percentualVariacao;
    }

    public void setPercentualVariacao(Double percentualVariacao) {
        this.percentualVariacao = percentualVariacao;
    }

    public String getAcao() {
        return acao;
    }

    public void setAcao(String acao) {
        this.acao = acao;
    }

    public EstufaModel getEstufaModel() {
        return estufaModel;
    }

    public void setEstufaModel(EstufaModel estufaModel) {
        this.estufaModel = estufaModel;
    }
}
