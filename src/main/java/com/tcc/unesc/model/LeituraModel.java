package com.tcc.unesc.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tcc.unesc.model.enums.IdentificacaoSensorEnum;

@Entity
@Table(name = "leituras")
public class LeituraModel implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idLeitura;
	private Double valorObtido;
	private String tipoLeitura;
	
	@ManyToOne
	@JoinColumn(name="idEstufa")
	private EstufaModel estufaModel;
	
	private Integer identificacaoSensorEnum; 
	
	@Basic(optional = false)
	@Column(name="timestamp", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern="dd/MM/yyyy HH:mm")
	private Date dataHora;

	public LeituraModel() {
	}

	public LeituraModel(Integer idLeitura, Double valorObtido, String tipoLeitura, EstufaModel estufaModel, IdentificacaoSensorEnum identificacaoSensorEnum, Date dataHora) {
		super();
		this.idLeitura = idLeitura;
		this.valorObtido = valorObtido;
		this.tipoLeitura = tipoLeitura;
		this.estufaModel = estufaModel;
		this.identificacaoSensorEnum = (identificacaoSensorEnum == null) ? null : identificacaoSensorEnum.getCod();
		this.dataHora = dataHora;
	}

	public Integer getIdLeitura() {
		return idLeitura;
	}

	public void setIdLeitura(Integer idLeitura) {
		this.idLeitura = idLeitura;
	}

	public Double getValorObtido() {
		return valorObtido;
	}

	public void setValorObtido(Double valorObtido) {
		this.valorObtido = valorObtido;
	}

	public String getTipoLeitura() {
		return tipoLeitura;
	}

	public void setTipoLeitura(String tipoLeitura) {
		this.tipoLeitura = tipoLeitura;
	}

	public EstufaModel getEstufaModel() {
		return estufaModel;
	}

	public void setEstufaModel(EstufaModel estufaModel) {
		this.estufaModel = estufaModel;
	}
	
	public IdentificacaoSensorEnum getIdentificacaoSensorEnum() {
		return IdentificacaoSensorEnum.toEnum(identificacaoSensorEnum);
	}

	public void setIdentificacaoSensorEnum(IdentificacaoSensorEnum identificacaoSensorEnum) {
		this.identificacaoSensorEnum = identificacaoSensorEnum.getCod();
	}

	public Date getDataHora() {
		return dataHora;
	}

	public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idLeitura == null) ? 0 : idLeitura.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LeituraModel other = (LeituraModel) obj;
		if (idLeitura == null) {
			if (other.idLeitura != null)
				return false;
		} else if (!idLeitura.equals(other.idLeitura))
			return false;
		return true;
	}
		
}
