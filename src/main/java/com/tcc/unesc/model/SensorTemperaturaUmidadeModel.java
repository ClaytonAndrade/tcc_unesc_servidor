package com.tcc.unesc.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "sensores_temperatura_umidade")
public class SensorTemperaturaUmidadeModel implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idSensor;
	private Double valorInformado;
	private Double percentualVariacao;
	private String acao;
	
	@ManyToOne
	@JoinColumn(name="idEstufa")
	private EstufaModel estufaModel;

	@Basic(optional = false)
	@Column(name="timestamp", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern="dd/MM/yyyy HH:mm")
	private Date dataHora;
		
	public SensorTemperaturaUmidadeModel() {
	}

	public SensorTemperaturaUmidadeModel(Integer idSensor, Double valorInformado, Double percentualVariacao,
			String acao, EstufaModel estufaModel, Date dataHora) {
		super();
		this.idSensor = idSensor;
		this.valorInformado = valorInformado;
		this.percentualVariacao = percentualVariacao;
		this.acao = acao;
		this.estufaModel = estufaModel;
		this.dataHora = dataHora;
	}

	public Integer getIdSensor() {
		return idSensor;
	}

	public void setIdSensor(Integer idSensor) {
		this.idSensor = idSensor;
	}

	public Double getValorInformado() {
		return valorInformado;
	}

	public void setValorInformado(Double valorInformado) {
		this.valorInformado = valorInformado;
	}

	public Double getPercentualVariacao() {
		return percentualVariacao;
	}

	public void setPercentualVariacao(Double percentualVariacao) {
		this.percentualVariacao = percentualVariacao;
	}

	public String getAcao() {
		return acao;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}

	public Date getDataHora() {
		return dataHora;
	}

	public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}
	
	public EstufaModel getEstufaModel() {
		return estufaModel;
	}

	public void setEstufaModel(EstufaModel estufaModel) {
		this.estufaModel = estufaModel;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idSensor == null) ? 0 : idSensor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SensorTemperaturaUmidadeModel other = (SensorTemperaturaUmidadeModel) obj;
		if (idSensor == null) {
			if (other.idSensor != null)
				return false;
		} else if (!idSensor.equals(other.idSensor))
			return false;
		return true;
	}
	
}
