package com.tcc.unesc.model.enums;

public enum IdentificacaoSensorEnum {
	
	SENSOR_PH(0, "Sensor de pH"),
	SENSOR_SOLO_1(1, "Sensor de solo 1"),
	SENSOR_SOLO_2(2, "Sensor de solo 2"),
	SENSOR_SOLO_3(3, "Sensor de solo 3"),
	SENSOR_TEMPERATURA_1(4, "Sensor de temperatura 1"),
	SENSOR_UMIDADE_1(5, "Sensor de umidade 1"),
	SENSOR_TEMPERATURA_2(6, "Sensor de temperatura 2"),
	SENSOR_UMIDADE_2(7, "Sensor de umidade 2"),
	SENSOR_TEMPERATURA_3(8, "Sensor de temperatura 3"),
	SENSOR_UMIDADE_3(9, "Sensor de umidade 3");

	private int cod;
	private String descricao;
	
	private IdentificacaoSensorEnum(int cod, String descricao) {
		this.cod = cod;
		this.descricao = descricao;
	}

	public int getCod() {
		return cod;
	}

	public String getDescricao() {
		return descricao;
	}

	public static IdentificacaoSensorEnum toEnum(Integer cod) {
		
		if (cod == null) {
			return null;
		}
		
		for (IdentificacaoSensorEnum n : IdentificacaoSensorEnum.values()) {
			if (cod.equals(n.getCod())) {
				return n;
			}
		}
		
		throw new IllegalArgumentException("Id inválido: " + cod);
	}

}
