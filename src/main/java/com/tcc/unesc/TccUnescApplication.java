package com.tcc.unesc;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.tcc.unesc.model.EstufaModel;
import com.tcc.unesc.model.LeituraModel;
import com.tcc.unesc.model.SensorPhModel;
import com.tcc.unesc.model.SensorSoloModel;
import com.tcc.unesc.model.SensorTemperaturaUmidadeModel;
import com.tcc.unesc.model.enums.IdentificacaoSensorEnum;
import com.tcc.unesc.repositories.EstufaRepository;
import com.tcc.unesc.repositories.LeituraRepository;
import com.tcc.unesc.repositories.SensorPhRepository;
import com.tcc.unesc.repositories.SensorSoloRepository;
import com.tcc.unesc.repositories.SensorTemperaturaUmidadeRepository;

@SpringBootApplication
public class TccUnescApplication implements CommandLineRunner{

	@Autowired
	private EstufaRepository estufaRepository;
	@Autowired
	private SensorPhRepository sensorPhRepository;
	@Autowired
	private SensorSoloRepository sensorSoloRepository;
	@Autowired
	private SensorTemperaturaUmidadeRepository sensorTemperaturaUmidadeRepository;
	@Autowired
	private LeituraRepository leituraRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(TccUnescApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

//		EstufaModel estufa1 = new EstufaModel(null, "Estufa Controlada");
//		EstufaModel estufa2 = new EstufaModel(null, "Estufa Natural");
//
//		SensorPhModel sensorPh = new SensorPhModel(1, 7.0, 0.2, "Informar operador", estufa1, null);
//
//		SensorSoloModel sensorSolo1 = new SensorSoloModel(1, 50.0, 0.2, "Regular Umidade", estufa1, null);
//		SensorSoloModel sensorSolo2 = new SensorSoloModel(2, 50.0, 0.2, "Regular Umidade", estufa2, null);
//
//		SensorTemperaturaUmidadeModel sensorTemperatura1 = new SensorTemperaturaUmidadeModel(1, 22.0, 0.2, "Regular Temperatura", estufa1, null);
//		SensorTemperaturaUmidadeModel sensorUmidade1     = new SensorTemperaturaUmidadeModel(2, 60.0, 0.2, "Regular Umidade", estufa1, null);
//		SensorTemperaturaUmidadeModel sensorTemperatura2 = new SensorTemperaturaUmidadeModel(3, 22.0, 0.2, "Regular Temperatura", estufa1, null);
//		SensorTemperaturaUmidadeModel sensorUmidade2     = new SensorTemperaturaUmidadeModel(4, 60.0, 0.2, "Regular Umidade", estufa1, null);
//		SensorTemperaturaUmidadeModel sensorTemperatura3 = new SensorTemperaturaUmidadeModel(5, 22.0, 0.2, "Temperatura fora do ideal", estufa2, null);
//		SensorTemperaturaUmidadeModel sensorUmidade3     = new SensorTemperaturaUmidadeModel(6, 60.0, 0.2, "Umidade fora do ideal", estufa2, null);

////		LeituraModel leituraModel1  =  new LeituraModel(null, 15.0, "temperatura",estufa1, IdentificacaoSensorEnum.SENSOR_TEMPERATURA_UMIDADE_1, null);
////		LeituraModel leituraModel2  =  new LeituraModel(null, 0.70, "umidade ar",estufa1, IdentificacaoSensorEnum.SENSOR_TEMPERATURA_UMIDADE_2, null);
////		LeituraModel leituraModel3  =  new LeituraModel(null, 15.0, "temperatura",estufa1, IdentificacaoSensorEnum.SENSOR_TEMPERATURA_UMIDADE_2, null);
////		LeituraModel leituraModel4  =  new LeituraModel(null, 0.70, "umidade ar",estufa1, IdentificacaoSensorEnum.SENSOR_TEMPERATURA_UMIDADE_1, null);
////		LeituraModel leituraModel5  =  new LeituraModel(null, 7.0, "pH água",estufa1, IdentificacaoSensorEnum.SENSOR_PH, null);
////		LeituraModel leituraModel6  =  new LeituraModel(null, 50.0, "umidade solo",estufa1, IdentificacaoSensorEnum.SENSOR_SOLO_1, null);
////		LeituraModel leituraModel7  =  new LeituraModel(null, 49.0, "umidade solo",estufa1, IdentificacaoSensorEnum.SENSOR_SOLO_2, null);
////		LeituraModel leituraModel8  =  new LeituraModel(null, 35.0, "umidade solo",estufa2, IdentificacaoSensorEnum.SENSOR_SOLO_3, null);
////		LeituraModel leituraModel9  =  new LeituraModel(null, 35.0, "temperatura",estufa2, IdentificacaoSensorEnum.SENSOR_TEMPERATURA_UMIDADE_3, null);
////		LeituraModel leituraModel10 =  new LeituraModel(null, 0.40, "umidade ar",estufa2, IdentificacaoSensorEnum.SENSOR_TEMPERATURA_UMIDADE_3, null);
//
//		estufaRepository.saveAll(Arrays.asList(estufa1, estufa2));
//		sensorPhRepository.saveAll(Arrays.asList(sensorPh));
//		sensorSoloRepository.saveAll(Arrays.asList(sensorSolo1,sensorSolo2));
//		sensorTemperaturaUmidadeRepository.saveAll(Arrays.asList(sensorTemperatura1, sensorUmidade1, sensorTemperatura2, sensorUmidade2, sensorTemperatura3, sensorUmidade3));
////		leituraRepository.saveAll(Arrays.asList(leituraModel1, leituraModel2, leituraModel3, leituraModel4, leituraModel5, leituraModel6, leituraModel7, leituraModel8, leituraModel9, leituraModel10));
	}

}
