package com.tcc.unesc.resources.excetion;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.tcc.unesc.services.exceptions.ObjectNotFoundExcetion;

@ControllerAdvice
public class ResourceExcetionHandler {

	@ExceptionHandler(ObjectNotFoundExcetion.class)
	public ResponseEntity<StandardError> objectNotFound(ObjectNotFoundExcetion e, HttpServletRequest request){
		
		long yourmilliseconds = System.currentTimeMillis();
		SimpleDateFormat sdf = new SimpleDateFormat("dd,MM,yyyy HH:mm");    
		Date resultdate = new Date(yourmilliseconds);
		System.out.println(sdf.format(resultdate) + " Aqui" );
		
		StandardError err = new StandardError(HttpStatus.NOT_FOUND.value(), e.getMessage(), resultdate.toString());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(err);
	}
}
