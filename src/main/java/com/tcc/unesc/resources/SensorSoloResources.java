package com.tcc.unesc.resources;

import java.util.List;

import com.tcc.unesc.model.SensorTemperaturaUmidadeModel;
import com.tcc.unesc.model.dto.SensorSoloDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tcc.unesc.model.SensorSoloModel;
import com.tcc.unesc.services.SensorSoloService;

@RestController
@RequestMapping(value="sensor-solo")
public class SensorSoloResources {
	
	@Autowired
	private SensorSoloService service;
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<SensorSoloModel> findById(@PathVariable Integer id){
		SensorSoloModel obj = service.find(id);
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<SensorSoloModel>> findAll(){
		List<SensorSoloModel> list = service.findAll();
		return ResponseEntity.ok().body(list);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public ResponseEntity<Void> update(@RequestBody SensorSoloDto objDto, @PathVariable Integer id){
		SensorSoloModel obj = service.fromDto(objDto);
		obj.setIdSensor(id);
		obj = service.update(obj);
		return ResponseEntity.noContent().build();
	}
}
