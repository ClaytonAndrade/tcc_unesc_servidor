package com.tcc.unesc.resources;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.tcc.unesc.model.LeituraModel;
import com.tcc.unesc.services.LeituraService;

@RestController
@RequestMapping(value="leitura")
public class LeituraResources {
	
	@Autowired
	private LeituraService service;
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<LeituraModel> findById(@PathVariable Integer id){
		LeituraModel obj = service.find(id);
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<LeituraModel>> findAll(){
		List<LeituraModel> list = service.findAll();
		return ResponseEntity.ok().body(list);
	}
	
	@RequestMapping(value="/max", method=RequestMethod.GET)
	public ResponseEntity<LeituraModel> findMax() {
		LeituraModel obj = service.findMax();
		return ResponseEntity.ok().body(obj);
	}
	@RequestMapping(value="/max-sensores-id", method=RequestMethod.GET)
	public ResponseEntity<List<LeituraModel>> findMaxSensoresId(
			@RequestParam(value="idEstufa", defaultValue="") Integer idEstufa) {
		List<LeituraModel> obj = service.findMaxById(idEstufa);
		return ResponseEntity.ok().body(obj);
	}

	@RequestMapping(value="/max-sensores", method=RequestMethod.GET)
	public ResponseEntity<List<LeituraModel>> findMaxSensores(
			@RequestParam(value="idEstufa", defaultValue="") Integer idEstufa) {
		List<LeituraModel> obj = service.findMaxSensoresId(idEstufa);
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public ResponseEntity<Void> update(@RequestBody LeituraModel obj, @PathVariable Integer id){
		obj.setIdLeitura(id);
		obj = service.update(obj);
		return ResponseEntity.noContent().build();
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Void> insert(@RequestBody LeituraModel obj){
		obj = service.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(obj.getIdLeitura()).toUri();
		return ResponseEntity.created(uri).build();
	}
}
