package com.tcc.unesc.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tcc.unesc.model.EstufaModel;
import com.tcc.unesc.services.EstufaService;

@RestController
@RequestMapping(value="estufa")
public class EstufaResources {
	
	@Autowired
	private EstufaService service;
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<EstufaModel> findById(@PathVariable Integer id){
		EstufaModel obj = service.find(id);
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<EstufaModel>> findAll(){
		List<EstufaModel> list = service.findAll();
		return ResponseEntity.ok().body(list);
	}
}
