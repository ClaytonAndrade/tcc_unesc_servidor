package com.tcc.unesc.resources;

import java.util.List;

import com.tcc.unesc.model.SensorSoloModel;
import com.tcc.unesc.model.dto.SensorPhDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tcc.unesc.model.SensorPhModel;
import com.tcc.unesc.services.SensorPhService;

@RestController
@RequestMapping(value="sensor-ph")
public class SensorPhResources {
	
	@Autowired
	private SensorPhService service;
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<SensorPhModel> findById(@PathVariable Integer id){
		SensorPhModel obj = service.find(id);
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<SensorPhModel>> findAll(){
		List<SensorPhModel> list = service.findAll();
		return ResponseEntity.ok().body(list);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public ResponseEntity<Void> update(@RequestBody SensorPhDto objDto, @PathVariable Integer id){
		SensorPhModel obj = service.fromDto(objDto);
		obj.setIdSensor(id);
		obj = service.update(obj);
		return ResponseEntity.noContent().build();
	}
}
