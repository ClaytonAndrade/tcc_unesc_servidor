package com.tcc.unesc.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tcc.unesc.model.LeituraModel;
import com.tcc.unesc.repositories.LeituraRepository;
import com.tcc.unesc.services.exceptions.ObjectNotFoundExcetion;

@Service
public class LeituraService {

	@Autowired
	private LeituraRepository repo;
	
	public LeituraModel find(Integer id) {
		Optional<LeituraModel> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundExcetion(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + LeituraModel.class.getName()));
	}
	
	public LeituraModel findMax(){
		Optional<LeituraModel> obj = repo.findById(repo.max());
		System.out.println(obj);
		return obj.orElseThrow(() -> new ObjectNotFoundExcetion(
				"Objeto não encontrado! Id: " + repo.findById(repo.max()) + ", Tipo: " + LeituraModel.class.getName()));
	}

	public List<LeituraModel> findMaxById(Integer idEstufa){
		List<LeituraModel> obj = new ArrayList<>();
		if(idEstufa == 1){
			obj.add(find(repo.maxSensoresIdIdentificacaoEnum(idEstufa, 0)));
			obj.add(find(repo.maxSensoresIdIdentificacaoEnum(idEstufa, 1)));
			obj.add(find(repo.maxSensoresIdIdentificacaoEnum(idEstufa, 4)));
			obj.add(find(repo.maxSensoresIdIdentificacaoEnum(idEstufa, 5)));
			obj.add(find(repo.maxSensoresIdIdentificacaoEnum(idEstufa, 6)));
			obj.add(find(repo.maxSensoresIdIdentificacaoEnum(idEstufa, 7)));
		}
		if(idEstufa == 2){
			obj.add(find(repo.maxSensoresIdIdentificacaoEnum(idEstufa, 2)));
			obj.add(find(repo.maxSensoresIdIdentificacaoEnum(idEstufa, 8)));
			obj.add(find(repo.maxSensoresIdIdentificacaoEnum(idEstufa, 9)));
		}
		return obj;
	}
	
	public List<LeituraModel> findMaxSensoresId(Integer idEstufa ){
		List<Integer> obj = repo.maxSensoresId(idEstufa);
		List<LeituraModel> lm = new ArrayList<>();
		for(int i =0; i<obj.size();i++){
			if(obj.get(i)!=null){
				lm.add(find(obj.get(i)));
				System.out.println(obj.get(i));
			}else {
				continue;
			}
		}
		return lm;//.orElseThrow(() -> new ObjectNotFoundExcetion(
				//"Objeto não encontrado! Id: " + repo.findById(repo.maxSensoresId(idEstufa)) + ", Tipo: " + LeituraModel.class.getName()));
	}

	public List<LeituraModel> findAll(){
		return repo.findAll();
	}
	
	public LeituraModel insert(LeituraModel obj) {
		obj.setIdLeitura(null);
		return repo.save(obj);
	}
	
	public LeituraModel update(LeituraModel obj) {
		LeituraModel newObj = find(obj.getIdLeitura());
		updateData(newObj, obj);
		return repo.save(newObj);
	}
	
	private void updateData(LeituraModel newObj, LeituraModel obj) {
		//não atualiza nada
	}

}
