package com.tcc.unesc.services.exceptions;

public class ObjectNotFoundExcetion extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	public ObjectNotFoundExcetion(String msg) {
		super(msg);
	}
	
	public ObjectNotFoundExcetion(String msg, Throwable cause) {
		super(msg, cause);
	}


}
