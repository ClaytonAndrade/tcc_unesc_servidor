package com.tcc.unesc.services;

import java.util.List;
import java.util.Optional;

import com.tcc.unesc.model.SensorSoloModel;
import com.tcc.unesc.model.SensorTemperaturaUmidadeModel;
import com.tcc.unesc.model.dto.SensorPhDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tcc.unesc.model.SensorPhModel;
import com.tcc.unesc.repositories.SensorPhRepository;
import com.tcc.unesc.services.exceptions.ObjectNotFoundExcetion;

@Service
public class SensorPhService {

	@Autowired
	private SensorPhRepository repo;
	
	public SensorPhModel find(Integer id) {
		Optional<SensorPhModel> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundExcetion(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + SensorPhModel.class.getName()));
	}
	
	public List<SensorPhModel> findAll(){
		return repo.findAll();
	}
	
	public SensorPhModel insert(SensorPhModel obj) {
		obj.setIdSensor(null);
		return repo.save(obj);
	}
	
	public SensorPhModel update(SensorPhModel obj) {
		SensorPhModel newObj = find(obj.getIdSensor());
		updateData(newObj, obj);
		return repo.save(newObj);
	}
	
	private void updateData(SensorPhModel newObj, SensorPhModel obj) {
		newObj.setValorInformado(obj.getValorInformado());
		newObj.setPercentualVariacao(obj.getPercentualVariacao());
	}

    public SensorPhModel fromDto(SensorPhDto objDto) {
		return new SensorPhModel(null, objDto.getValorInformado(), objDto.getPercentualVariacao(), objDto.getAcao(), objDto.getEstufaModel(), null );
	}
}
