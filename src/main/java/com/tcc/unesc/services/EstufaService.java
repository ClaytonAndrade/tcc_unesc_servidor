package com.tcc.unesc.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tcc.unesc.model.EstufaModel;
import com.tcc.unesc.repositories.EstufaRepository;
import com.tcc.unesc.services.exceptions.ObjectNotFoundExcetion;

@Service
public class EstufaService {

	@Autowired
	private EstufaRepository repo;
	
	public EstufaModel find(Integer id) {
		Optional<EstufaModel> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundExcetion(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + EstufaModel.class.getName()));
	}
	
	public List<EstufaModel> findAll(){
		return repo.findAll();
	}
	
	public EstufaModel insert(EstufaModel obj) {
		obj.setIdEstufa(null);
		return repo.save(obj);
	}
	
}
