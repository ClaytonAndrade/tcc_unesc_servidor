package com.tcc.unesc.services;

import java.util.List;
import java.util.Optional;

import com.tcc.unesc.model.SensorPhModel;
import com.tcc.unesc.model.dto.SensorSoloDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tcc.unesc.model.SensorSoloModel;
import com.tcc.unesc.repositories.SensorSoloRepository;
import com.tcc.unesc.services.exceptions.ObjectNotFoundExcetion;

@Service
public class SensorSoloService {

	@Autowired
	private SensorSoloRepository repo;
	
	public SensorSoloModel find(Integer id) {
		Optional<SensorSoloModel> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundExcetion(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + SensorSoloModel.class.getName()));
	}
	
	public List<SensorSoloModel> findAll(){
		return repo.findAll();
	}
	
	public SensorSoloModel insert(SensorSoloModel obj) {
		obj.setIdSensor(null);
		return repo.save(obj);
	}
	
	public SensorSoloModel update(SensorSoloModel obj) {
		SensorSoloModel newObj = find(obj.getIdSensor());
		updateData(newObj, obj);
		return repo.save(newObj);
	}
	
	private void updateData(SensorSoloModel newObj, SensorSoloModel obj) {
		newObj.setValorInformado(obj.getValorInformado());
		newObj.setPercentualVariacao(obj.getPercentualVariacao());
	}

    public SensorSoloModel fromDto(SensorSoloDto objDto) {
		return new SensorSoloModel(null, objDto.getValorInformado(), objDto.getPercentualVariacao(), objDto.getAcao(), objDto.getEstufaModel(), null );
	}
}
