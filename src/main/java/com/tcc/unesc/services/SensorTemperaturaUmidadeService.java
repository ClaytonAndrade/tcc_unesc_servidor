package com.tcc.unesc.services;

import java.util.List;
import java.util.Optional;

import com.tcc.unesc.model.dto.SensorTemperaturaUmidadeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tcc.unesc.model.SensorTemperaturaUmidadeModel;
import com.tcc.unesc.repositories.SensorTemperaturaUmidadeRepository;
import com.tcc.unesc.services.exceptions.ObjectNotFoundExcetion;

@Service
public class SensorTemperaturaUmidadeService {

	@Autowired
	private SensorTemperaturaUmidadeRepository repo;
	
	public SensorTemperaturaUmidadeModel find(Integer id) {
		Optional<SensorTemperaturaUmidadeModel> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundExcetion(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + SensorTemperaturaUmidadeModel.class.getName()));
	}
	
	public List<SensorTemperaturaUmidadeModel> findAll(){
		return repo.findAll();
	}
	
	public SensorTemperaturaUmidadeModel insert(SensorTemperaturaUmidadeModel obj) {
		obj.setIdSensor(null);
		return repo.save(obj);
	}
	
	public SensorTemperaturaUmidadeModel update(SensorTemperaturaUmidadeModel obj) {
		SensorTemperaturaUmidadeModel newObj = find(obj.getIdSensor());
		updateData(newObj, obj);
		return repo.save(newObj);
	}
	
	private void updateData(SensorTemperaturaUmidadeModel newObj, SensorTemperaturaUmidadeModel obj) {
		newObj.setValorInformado(obj.getValorInformado());
		newObj.setPercentualVariacao(obj.getPercentualVariacao());
	}

    public SensorTemperaturaUmidadeModel fromDto(SensorTemperaturaUmidadeDto objDto) {
		return new SensorTemperaturaUmidadeModel(null, objDto.getValorInformado(), objDto.getPercentualVariacao(), objDto.getAcao(), objDto.getEstufaModel(), null );
    }
}
